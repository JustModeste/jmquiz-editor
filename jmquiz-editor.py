from app import app, db
from app.models.user import User
from app.models.language import Language
from app.models.category import Category
from app.models.force import Force
from app.models.question import Question

@app.shell_context_processor
def make_shell_context():
	return {'db': db, 'User': User, 'Category': Category}