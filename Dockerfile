FROM python:3.9-alpine

RUN adduser -D jmquizeditor
RUN apk add gcc musl-dev
WORKDIR /home/just/jmquiz-editor

COPY requirements.txt requirements.txt
RUN python -m venv venv
# RUN venv/bin/python -m pip install --upgrade pip
RUN venv/bin/pip install -r requirements.txt
RUN venv/bin/pip install gunicorn

COPY app app
COPY migrations migrations
COPY jmquiz-editor.py config.py boot.sh ./
RUN chmod +x boot.sh

ENV FLASK_APP jmquiz-editor.py

RUN chown -R jmquizeditor:jmquizeditor ./
USER jmquizeditor

EXPOSE 5000
ENTRYPOINT ["./boot.sh"]