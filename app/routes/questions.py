from flask import render_template, flash, redirect, url_for, request, jsonify
from flask_login import current_user, login_user, logout_user, login_required
from app import app
from app.models.question import Question
from app.models.answer import Answer
from app.models.language import Language
from app.models.category import Category
from app.models.force import Force

from datetime import datetime

@app.route("/questions", methods=["GET", "POST"])
def questions():
	if request.form:
		currentLanguage = request.form.get("current_language")
	else :
		currentLanguage = request.args.get("current_language")

	if currentLanguage is not None:
		languageId = int(currentLanguage)
	else:
		languageId = 1

	questions = Question.getAllForLanguage(languageId)
	languages = Language.getAll()

	return render_template(
		'questions.html', 
		questions = questions, 
		currentLanguage = languageId,
		languages = languages
	)

@app.route('/getquestions', methods =["GET"])
def getQuestions():
	language = request.args.get("languageid")
	lastSynchro = request.args.get("lastsynchro")

	if lastSynchro is None:
		lastSynchro = "2020-01-01"

	questions = Question.getAllAfterDate(lastSynchro, language)
	return jsonify({"questions": [question.toJSON2() for question in questions]})

@app.route('/questions/edit/<questionid>', methods = ["GET", "POST"])
def editQuestion(questionid):
	currentLanguage = request.args.get("language")
	print(request)
	questionid = int(questionid)
	print("questionID= " + str(questionid))
	if questionid == 0:
		question = Question()
	else:
		question = Question.findById(questionid)

	print("correct:")
	print(question.answer_correct)

	categories = Category.getAllForLanguage(currentLanguage)
	forces = Force.getAllForLanguage(currentLanguage)
	answers = Answer.getAllForLanguage(currentLanguage)
	language = Language.findById(currentLanguage)

	print(categories)
	return render_template(
		'editquestion.html',
		question = question,
		categories = categories,
		forces = forces,
		answers = answers,
		language = language
	)

@app.route('/questions/validquestion/', methods = ["POST"])
def validQuestion():
	id_question = request.form.get("question_id")
	if id_question is not None:
		id_question = int(id_question)
	
	id_language = int(request.form.get("current_language"))
	id_categorie = int(request.form.get("categories"))
	id_force = int(request.form.get("forces"))

	label_question = request.form.get("label_question")
	answer1 = request.form.get("answer1")
	answer2 = request.form.get("answer2")
	answer3 = request.form.get("answer3")
	answer4 = request.form.get("answer4")
	correct = int(request.form.get("correct"))

	idAnswer1 = Answer(answer1, id_language).update()
	idAnswer2 = Answer(answer2, id_language).update()
	idAnswer3 = Answer(answer3, id_language).update()
	idAnswer4 = Answer(answer4, id_language).update()

	idCorrect = idAnswer1

	if correct == 2:
		idCorrect = idAnswer2
	elif correct == 3:
		idCorrect = idAnswer3
	elif correct == 4:
		idCorrect = idAnswer4

	if id_question is not None and id_question > 0:
		question = Question.findById(id_question)
	else:
		question = Question()

	question.label = label_question
	question.answer1_id = idAnswer1
	question.answer2_id = idAnswer2
	question.answer3_id = idAnswer3
	question.answer4_id = idAnswer4
	question.answer_correct = idCorrect
	question.category_id = id_categorie
	question.force_id = id_force
	question.language_id = id_language
	question.updated_by = current_user.get_id()

	question.update()
	return redirect(url_for('questions', current_language = id_language))