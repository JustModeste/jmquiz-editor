#
# Route Forces
#
from flask import render_template, redirect, request, url_for
from app import app, db
from app.models.force import Force
from app.models.language import Language

@app.route('/forces', methods=["GET", "POST"])
def forces():
	if request.form:
		currentLanguage = request.form.get("current_language")
	else :
		currentLanguage = request.args.get("current_language")

	if currentLanguage is not None:
		languageId = int(currentLanguage)
	else:
		languageId = 1


	forces = Force.getAllForLanguage(languageId)
	languages = Language.getAll()

	return render_template(
		'forces.html', 
		forces = forces, 
		currentLanguage = languageId,
		languages = languages
	)

@app.route("/forces/update", methods=["POST"])
def updateForce():
	label = request.form.get("label")
	if label != "":
		languageId = request.form.get("current_language")
		forceId = request.form.get("force_id")
		if forceId == "":
			force = Force(label= label, language_id= languageId)
			force.update()
		else:
			force = Force.findById(forceId)
			if force is not None:
				force.label = label
				force.update()
	return redirect(url_for('forces', current_language= languageId))

@app.route("/forces/delete", methods=["POST"])
def deleteForce():
	languageId = int(request.form.get("current_language"))
	forceId = int(request.form.get("force_id"))

	force = Force.findById(forceId)
	if force is not None:
		force.delete()
	return redirect(url_for('forces', current_language= languageId))