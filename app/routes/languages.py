#
# Routes Langues
#
from flask import render_template, redirect, url_for, request
from app import app, db
from app.models.language import Language

@app.route("/languages/", methods=["GET", "POST"])
def languages():
	languages = Language.getAll()
	return render_template("languages.html", languages = languages)

@app.route("/languages/update", methods=["POST"])
def updateLanguage():
	label = request.form.get("label").title()
	if label != "":
		languageId = request.form.get("language_id")
		if languageId == "":
			language = Language(label)
			language.update()
		else:
			language = Language.findById(languageId)
			if language is not None:
				language.label = label
				language.update()
	return redirect(url_for('languages'))

@app.route("/languages/delete", methods=["POST"])
def deleteLanguage():
	languageId = int(request.form.get("language_id"))

	language = Language.findById(languageId)
	if language is not None:
		language.delete()
	return redirect(url_for('languages'))