#
# Route Categories
#
from flask import render_template, redirect, request, url_for
from app import app, db
from app.models.category import Category
from app.models.language import Language

@app.route('/categories', methods=["GET", "POST"])
def categories():
	if request.form:
		currentLanguage = request.form.get("current_language")
	else :
		currentLanguage = request.args.get("current_language")

	if currentLanguage is not None:
		languageId = int(currentLanguage)
	else:
		languageId = 1

	categories = Category.getAllForLanguage(languageId)
	languages = Language.getAll()

	return render_template(
		'categories.html', 
		categories = categories, 
		currentLanguage = languageId,
		languages = languages
	)

@app.route("/categories/update", methods=["POST"])
def updateCategory():
	label = request.form.get("label")
	if label != "":
		languageId = request.form.get("current_language")
		categoryId = request.form.get("category_id")
		if categoryId == "":
			category = Category(label= label, language_id= languageId)
			category.update()
		else:
			category = Category.findById(categoryId)
			if category is not None:
				category.label = label
				category.update()
	return redirect(url_for('categories', current_language= languageId))

@app.route("/categories/delete", methods=["POST"])
def deleteCategory():
	languageId = int(request.form.get("current_language"))
	categoryId = int(request.form.get("category_id"))

	category = Category.findById(categoryId)
	if category is not None:
		category.delete()
	return redirect(url_for('categories', current_language= languageId))