#
# Route Answers
#
from flask import render_template, redirect, request, url_for
from app import app, db
from app.models.answer import Answer
from app.models.language import Language

@app.route("/answers", methods=["GET", "POST"])
def answers():
	if request.form:
		currentLanguage = request.form.get("current_language")
	else :
		currentLanguage = request.args.get("current_language")

	if currentLanguage is not None:
		languageId = int(currentLanguage)
	else:
		languageId = 1

	answers = Answer.getAllForLanguage(languageId)
	languages = Language.getAll()

	return render_template(
		'answers.html', 
		answers = answers, 
		currentLanguage = languageId,
		languages = languages
	)

@app.route("/answers/update", methods=["POST"])
def updateAnswer():
	label = request.form.get("label")
	if label != "":
		languageId = request.form.get("current_language")
		answerId = request.form.get("answer_id")
		if answerId == "":
			answer = Answer(label= label, language_id= languageId)
			answer.update()
		else:
			answer = Answer.findById(answerId)
			if answer is not None:
				answer.label = label
				answer.update()
	return redirect(url_for('answers', current_language= languageId))

@app.route("/answers/delete", methods=["POST"])
def deleteAnswer():
	languageId = int(request.form.get("current_language"))
	answerId = int(request.form.get("answer_id"))

	answer = Answer.findById(answerId)
	if answer is not None:
		answer.delete()
	return redirect(url_for('answers', current_language= languageId))