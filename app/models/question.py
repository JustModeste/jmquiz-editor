from app import db
from app.models.category import Category
from app.models.force import Force
from app.models.language import Language
from app.models.answer import Answer

from datetime import datetime
from sqlalchemy import and_

class Question(db.Model):
	id = db.Column(db.Integer, primary_key = True)
	label = db.Column(db.String(255), unique = True)
	category_id = db.Column(db.Integer, db.ForeignKey('category.id'), nullable = False)
	force_id = db.Column(db.Integer, db.ForeignKey('force.id'), nullable = False)
	language_id = db.Column(db.Integer, db.ForeignKey('language.id'), nullable = False)
	answer1_id = db.Column(db.Integer, db.ForeignKey('answer.id'),nullable = False)
	answer2_id = db.Column(db.Integer, db.ForeignKey('answer.id'),nullable = False)
	answer3_id = db.Column(db.Integer, db.ForeignKey('answer.id'),nullable = False)
	answer4_id = db.Column(db.Integer, db.ForeignKey('answer.id'),nullable = False)
	answer_correct = db.Column(db.Integer, db.ForeignKey('answer.id'),nullable = False)
	updated_date = db.Column(db.DateTime, default= datetime.utcnow())
	updated_by = db.Column(db.Integer, db.ForeignKey('user.id'), nullable = False)

	def __init__(self, label = "", category_id = -1, force_id = -1, language_id = -1, 
		answer1_id = -1, answer2_id = -1, answer3_id = -1, answer4_id = -1, answer_correct = -1, updated_by = -1):
		self.id = 0
		self.label = label
		self.category_id = category_id
		self.force_id = force_id
		self.language_id = language_id
		self.answer1_id = answer1_id
		self.answer2_id = answer2_id
		self.answer3_id = answer3_id
		self.answer4_id = answer4_id
		self.answer_correct = answer_correct
		self.updated_by = updated_by	
		
	def __repr__(self):
		return '<Question {}>'.format(self.label)

	def getAll():
		return db.session.query(Question).all()

	def getAllAfterDate(date, languageId):
		if int(languageId) == 0:
			return db.session.query(Question).filter(Question.updated_date >= date).all()
		return db.session.query(Question).filter(and_(Question.updated_date >= date, Question.language_id == languageId)).all()

	def getAllForLanguage(language_id):
		return db.session.query(Question).filter_by(language_id = language_id).all()

	def findById(questionId):
		return db.session.query(Question).filter_by(id = questionId).first()

	def findByLabel(label):
		return db.session.query(Question).filter_by(label = label).first()

	def getAnswer1Str(self):
		answer = Answer.findById(self.answer1_id)
		if answer is None:
			return ""
		return answer.label

	def getAnswer2Str(self):
		answer = Answer.findById(self.answer2_id)
		if answer is None:
			return ""
		return answer.label

	def getAnswer3Str(self):
		answer = Answer.findById(self.answer3_id)
		if answer is None:
			return ""
		return answer.label

	def getAnswer4Str(self):
		answer = Answer.findById(self.answer4_id)
		if answer is None:
			return ""
		return answer.label

	def update(self):
		print("ici")
		if self.label != "":
			question = Question.findByLabel(self.label)
				
			if question is not None:
				self.id = question.id
				
			if self.id == 0:
				self.id = None
				db.session.add(self)

			self.updated_date = datetime.utcnow()
			db.session.commit()
		return self.id

	def delete(self):
		if self.id is not None:
			db.session.delete(self)
			db.session.commit()

	def toJSON(self):
		return {
			'id': self.id,
			'label': self.label,
			'category': Category.findById(self.category_id).toJSON(),
			'force': Force.findById(self.force_id).toJSON(),
			'language': Language.findById(self.language_id).toJSON(),
			'answer1': Answer.findById(self.answer1_id).toJSON(),
			'answer2': Answer.findById(self.answer2_id).toJSON(),
			'answer3': Answer.findById(self.answer3_id).toJSON(),
			'answer4': Answer.findById(self.answer4_id).toJSON(),
			'updated_date': format(self.updated_date),
			'correct': self.answer_correct
		}

	def toJSON2(self):
		return {
			"QST_ID": self.id,
			"QST_LABEL":  self.label,
			"ANS_ID_1": self.answer1_id,
			"ANS_LABEL_1": Answer.findById(self.answer1_id).label,
			"CORRECT_1": int(self.answer1_id == self.answer_correct),
			"ANS_ID_2": self.answer2_id,
			"ANS_LABEL_2": Answer.findById(self.answer2_id).label,
			"CORRECT_2": int(self.answer2_id == self.answer_correct),
			"ANS_ID_3": self.answer3_id,
			"ANS_LABEL_3": Answer.findById(self.answer3_id).label,
			"CORRECT_3": int(self.answer3_id == self.answer_correct),
			"ANS_ID_4": self.answer4_id,
			"ANS_LABEL_4": Answer.findById(self.answer4_id).label,
			"CORRECT_4": int(self.answer4_id == self.answer_correct),
		}