from app import db
from datetime import datetime
from app.models.language import Language

class Force(db.Model):
	id = db.Column(db.Integer, primary_key = True)
	label = db.Column(db.String(50), unique = True)
	is_referential = db.Column(db.Boolean, default = False)
	language_id = db.Column(db.Integer, db.ForeignKey('language.id'), nullable = False)
	updated_date = db.Column(db.DateTime, default= datetime.utcnow)

	def __init__(self, label, language_id, is_referential = False):
		self.label = label
		self.language_id = language_id
		self.is_referential = is_referential

	def __repr__(self):
		return '<Force {}>'.format(self.label)

	def getAll():
		return db.session.query(Force).all()

	def getAllForLanguage(language_id):
		return db.session.query(Force).filter_by(language_id = language_id).order_by(Force.label).all()

	def findById(categoryId):
		return db.session.query(Force).filter_by(id = categoryId).first()

	def findByLabel(label):
		return db.session.query(Force).filter_by(label = label).first()

	def update(self):
		if self.label != "":
			if self.id is None:
				force = Force.findByLabel(self.label)
				if force is not None:
					self.id = answer.id
			if self.id is None:
				db.session.add(self)

			self.updated_date = datetime.utcnow()
			db.session.commit()
		return self.id

	def delete(self):
		if self.id is not None:
			db.session.delete(self)
			db.session.commit()

	def toJSON(self):
		return {
			'id': self.id,
			'label': self.label,
			'language': Language.findById(self.language_id).toJSON()
		}