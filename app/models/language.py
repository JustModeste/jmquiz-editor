from app import db
from datetime import datetime

class Language(db.Model):
	id = db.Column(db.Integer, primary_key = True)
	label = db.Column(db.String(50), unique = True)
	is_referential = db.Column(db.Boolean, default = False)
	updated_date = db.Column(db.DateTime, default= datetime.utcnow)
	category = db.relationship('Category', backref='Language', lazy = True)
	force = db.relationship('Force', backref='Language', lazy = True)
	answer = db.relationship('Answer', backref='Language', lazy = True)

	def __init__(self, label, is_referential = False):
		self.label = label
		self.is_referential = is_referential

	def __repr__(self):
		return '<Language {}>'.format(self.label)

	def getAll():
		return db.session.query(Language).order_by(Language.label).all()

	def findById(languageId):
		return db.session.query(Language).filter_by(id = languageId).first()

	def findByLabel(label):
		return db.session.query(Language).filter_by(label = label).first()

	def update(self):
		if self.label != "":
			if self.id is None:
				language = Language.findByLabel(self.label)
				if language is not None:
					self.id = answer.id
			if self.id is None:
				db.session.add(self)

			self.updated_date = datetime.utcnow()
			db.session.commit()
		return self.id


	def delete(self):
		if self.id is not None:
			db.session.delete(self)
			db.session.commit()

	def toJSON(self):
		return {
			'id': self.id,
			'label': self.label
		}