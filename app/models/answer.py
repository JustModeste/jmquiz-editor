from app import db
from datetime import datetime
from app.models.language import Language

class Answer(db.Model):
	id = db.Column(db.Integer, primary_key = True)
	label = db.Column(db.String(50), unique = True)
	language_id = db.Column(db.Integer, db.ForeignKey('language.id'), nullable = False)
	updated_date = db.Column(db.DateTime, default= datetime.utcnow)

	def __init__(self, label, language_id, is_referential = False):
		self.label = label
		self.language_id = language_id
		self.is_referential = is_referential

	def __repr__(self):
		return '<Answer {}>'.format(self.label)

	def getAll():
		return db.session.query(Answer).all()

	def getAllForLanguage(language_id):
		return db.session.query(Answer).filter_by(language_id = language_id).order_by(Answer.label).all()

	def findById(answerId):
		return db.session.query(Answer).filter_by(id = answerId).first()

	def findByLabel(label):
		return db.session.query(Answer).filter_by(label = label).first()

	def update(self):
		if self.label != "":
			if self.id is None:
				answer = Answer.findByLabel(self.label)
				if answer is not None:
					self.id = answer.id
			if self.id is None:
				db.session.add(self)

			self.updated_date = datetime.utcnow()
			db.session.commit()
		return self.id

	def delete(self):
		if self.id is not None:
			db.session.delete(self)
			db.session.commit()

	def toJSON(self):
		return {
			'id': self.id,
			'label': self.label,
			'language': Language.findById(self.language_id).toJSON()
		}