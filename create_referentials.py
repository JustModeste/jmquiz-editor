from app import app, db
from app.models.language import Language
from app.models.force import Force
from app.models.category import Category
from app.models.question import Question
from app.models.answer import Answer

def createLanguages():
	print("Create Languagues...")

	languages = [
		Language("Français", True),
		Language("English", True),
		Language("Portugues", True)
	]

	for language in languages:
		if Language.findByLabel(language.label) is None:
			print(language)
			language.update()

	print("Languages are OK.")

def forcesForFench(language):
	return [
		Force("Enfant", language.id, True),
		Force("Facile", language.id, True),
		Force("Moyen", language.id, True),
		Force("Difficile", language.id, True)
	]

def forcesForEnglish(language):
	return [
		Force("Kind", language.id, True),
		Force("Easy", language.id, True),
		Force("Medium", language.id, True),
		Force("Hard", language.id, True)
	]

def forcesForPortugues():
	return []

def createForcesForLanguage(language):
	print("... For " + language.label)
	forces = []

	if language.label == "Français":
		forces = forcesForFench(language)

	if language.label == "English":
		forces = forcesForEnglish(language)

	if forces is not None:
		for force in forces:
			if Force.findByLabel(force.label) is None:
				print(force)
				force.update()

def createForces(languages):
	print("Create Forces...")
	for language in languages:
		createForcesForLanguage(language)

	print("... OK.")

def categoriesForFrench(language):
	return [
		Category("Enfant", language.id, True),
		Category("Politique", language.id, True),
		Category("Histoire", language.id, True),
		Category("Culture générale", language.id, True),
		Category("Sport", language.id, True),
		Category("Géographie", language.id, True),
		Category("Faune et Flore", language.id, True),
		Category("Cinéma", language.id, True),
		Category("Musique", language.id, True),
		Category("Divertissement", language.id, True)
	]

def createCategoriesForLanguage(language):
	print("... For " + language.label)
	categories = []

	if language.label == "Français":
		categories = categoriesForFrench(language)

	if categories is not None:
		for category in categories:
			if Category.findByLabel(category.label) is None:
				print(category)
				category.update()

def createCategories(languages):
	print("Create Categories...")
	for language in languages:
		createCategoriesForLanguage(language)

	print("... OK.")

def questionsForFrench(language):
	questions = []
	
	idAnswer1 = Answer("Chausson", language.id).update()
	idAnswer2 = Answer("Soulier", language.id).update()
	idAnswer3 = Answer("Babouche", language.id).update()
	idAnswer4 = Answer("Mule", language.id).update()
	
	question = Question(
		"Comment s'appelle le singe, compagnon de Dora l'exploratrice ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
		idAnswer1,
		idAnswer2,
		idAnswer3,
		idAnswer4,
		idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Edith CRESSON", language.id).update()
	idAnswer2 = Answer("Ségolène ROYAL", language.id).update()
	idAnswer3 = Answer("Simone VEIL", language.id).update()
	idAnswer4 = Answer("Michèle ALLIOT-MARIE", language.id).update()

	question = Question(
		"En France, qui a été la première femme 1er Ministre ?",
		(Category.findByLabel("Politique")).id,
		(Force.findByLabel("Moyen")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer1
	)
	questions.append(question)

	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)
	
	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Raichu", language.id).update()
	idAnswer2 = Answer("Pichu", language.id).update()
	idAnswer3 = Answer("Racaillou", language.id).update()
	idAnswer4 = Answer("Voltali", language.id).update()

	question = Question(
		"Quelle est l'évolution du pokémon Pikachu ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer1
	)
	questions.append(question)

	idAnswer1 = Answer("20", language.id).update()
	idAnswer2 = Answer("30", language.id).update()
	idAnswer3 = Answer("40", language.id).update()
	idAnswer4 = Answer("50", language.id).update()

	question = Question(
		"Combien y-a-t-il de cases sur le plateau de Monopoly ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Quelconque", language.id).update()
	idAnswer2 = Answer("Isocèle", language.id).update()
	idAnswer3 = Answer("Équilatéral", language.id).update()
	idAnswer4 = Answer("Rectangle", language.id).update()

	question = Question(
		"Un triangle avec 2 cotés de mêmes longueurs est un triangle ?",
		(Category.findByLabel("Culture générale")).id,
		(Force.findByLabel("Facile")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer2
	)
	questions.append(question)

	idAnswer1 = Answer("Quelconque", language.id).update()
	idAnswer2 = Answer("Isocèle", language.id).update()
	idAnswer3 = Answer("Équilatéral", language.id).update()
	idAnswer4 = Answer("Rectangle", language.id).update()

	question = Question(
		"Un triangle avec 3 cotés de mêmes longueurs est un triangle ?",
		(Category.findByLabel("Culture générale")).id,
		(Force.findByLabel("Facile")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Quelconque", language.id).update()
	idAnswer2 = Answer("Isocèle", language.id).update()
	idAnswer3 = Answer("Équilatéral", language.id).update()
	idAnswer4 = Answer("Rectangle", language.id).update()
	
	question = Question(
		"Un triangle avec 1 angle droit est un triangle ?",
		(Category.findByLabel("Culture générale")).id,
		(Force.findByLabel("Facile")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer4
	)
	questions.append(question)

	idAnswer1 = Answer("Louis XVI", language.id).update()
	idAnswer2 = Answer("Louis XVIII", language.id).update()
	idAnswer3 = Answer("Charles X", language.id).update()
	idAnswer4 = Answer("Louis-Philippe Ier", language.id).update()

	question = Question(
		"Qui a été le dernier roi en France ?",
		(Category.findByLabel("Histoire")).id,
		(Force.findByLabel("Difficile")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer1
	)
	questions.append(question)

	idAnswer1 = Answer("12", language.id).update()
	idAnswer2 = Answer("7", language.id).update()
	idAnswer3 = Answer("9", language.id).update()
	idAnswer4 = Answer("11", language.id).update()

	question = Question(
		"Dans une année bissextile, combien de mois compte 31 jours ?",
		(Category.findByLabel("Culture générale")).id,
		(Force.findByLabel("Facile")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer2
	)
	questions.append(question)
	
	idAnswer1 = Answer("364", language.id).update()
	idAnswer2 = Answer("365", language.id).update()
	idAnswer3 = Answer("366", language.id).update()
	idAnswer4 = Answer("367", language.id).update()

	question = Question(
		"Combien y-a-t-il de jours pendant une année bissextile ?",
		(Category.findByLabel("Culture générale")).id,
		(Force.findByLabel("Facile")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("364", language.id).update()
	idAnswer2 = Answer("365", language.id).update()
	idAnswer3 = Answer("366", language.id).update()
	idAnswer4 = Answer("367", language.id).update()

	question = Question(
		"Combien y-a-t-il de jours pendant une année non bissextile ?",
		(Category.findByLabel("Culture générale")).id,
		(Force.findByLabel("Facile")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer2
	)
	questions.append(question)

	idAnswer1 = Answer("Orne", language.id).update()
	idAnswer2 = Answer("Tarn", language.id).update()
	idAnswer3 = Answer("Loir-et-Cher", language.id).update()
	idAnswer4 = Answer("Côte-d'Or", language.id).update()

	question = Question(
		"Quel département Français correspond à l'opération : ((9 * 2) - (4 + 5)) * ((5 * 3) - (12 / 2))  ?",
		(Category.findByLabel("Géographie")).id,
		(Force.findByLabel("Difficile")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer2
	)
	questions.append(question)

	idAnswer1 = Answer("Des coquetiers", language.id).update()
	idAnswer2 = Answer("Des coquillages", language.id).update()
	idAnswer3 = Answer("Des porte-clés", language.id).update()
	idAnswer4 = Answer("Des cagoules", language.id).update()

	question = Question(
		"Que collectionne un copocléphile ?",
		(Category.findByLabel("Culture générale")).id,
		(Force.findByLabel("Difficile")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Thierry HENRY", language.id).update()
	idAnswer2 = Answer("Zinedine ZIDANE", language.id).update()
	idAnswer3 = Answer("David TRÉZÉGUET", language.id).update()
	idAnswer4 = Answer("Emmanuel PETIT", language.id).update()

	question = Question(
		"Qui a marqué le dernier but de la Coupe du Monde de football 1998 ?",
		(Category.findByLabel("Sport")).id,
		(Force.findByLabel("Difficile")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer4
	)
	questions.append(question)

	idAnswer1 = Answer("France", language.id).update()
	idAnswer2 = Answer("Espagne", language.id).update()
	idAnswer3 = Answer("Suède", language.id).update()
	idAnswer4 = Answer("Allemagne", language.id).update()
	
	question = Question(
		"Quel pays de l'Union Européenne possède la plus grande superficie ?",
		(Category.findByLabel("Géographie")).id,
		(Force.findByLabel("Moyen")).id,
		language.id,
		idAnswer1,
		idAnswer2,
		idAnswer3,
		idAnswer4,
		idAnswer1
	)
	questions.append(question)

	idAnswer1 = Answer("95", language.id).update()
	idAnswer2 = Answer("96", language.id).update()
	idAnswer3 = Answer("100", language.id).update()
	idAnswer4 = Answer("101", language.id).update()

	question = Question(
		"En 2020, combien de départements compte la France ?",
		(Category.findByLabel("Politique")).id,
		(Force.findByLabel("Moyen")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer4
	)
	questions.append(question)

	idAnswer1 = Answer("Pas-de-Calais", language.id).update()
	idAnswer2 = Answer("Aisne", language.id).update()
	idAnswer3 = Answer("Somme", language.id).update()
	idAnswer4 = Answer("Oise", language.id).update()

	question = Question(
		"En France, Quel département n'est pas limitrophe avec le Nord ?",
		(Category.findByLabel("Géographie")).id,
		(Force.findByLabel("Moyen")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer4
	)
	questions.append(question)
	
	idAnswer1 = Answer("Un bug", language.id).update()
	idAnswer2 = Answer("Une bogue", language.id).update()
	idAnswer3 = Answer("La coque", language.id).update()
	idAnswer4 = Answer("La capsule", language.id).update()

	question = Question(
		"Comment s'appelle l'enveloppe hérissée de piquants qui protège les chataignes ?",
		(Category.findByLabel("Faune et Flore")).id,
		(Force.findByLabel("Moyen")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer2
	)
	questions.append(question)

	idAnswer1 = Answer("Inspecteur", language.id).update()
	idAnswer2 = Answer("Commissaire", language.id).update()
	idAnswer3 = Answer("Lieutenant", language.id).update()
	idAnswer4 = Answer("Capitaine", language.id).update()

	question = Question(
		"Dans la série, quel était le grade de Columbo ?",
		(Category.findByLabel("Cinéma")).id,
		(Force.findByLabel("Moyen")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Allemagne", language.id).update()
	idAnswer2 = Answer("Belgique", language.id).update()
	idAnswer3 = Answer("Suisse", language.id).update()
	idAnswer4 = Answer("Autriche", language.id).update()

	question = Question(
		"Quel pays n'a pas de frontière commune avec la France ?",
		(Category.findByLabel("Géographie")).id,
		(Force.findByLabel("Moyen")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer4
	)
	questions.append(question)

	idAnswer1 = Answer("Madrid", language.id).update()
	idAnswer2 = Answer("Barcelone", language.id).update()
	idAnswer3 = Answer("Valence", language.id).update()
	idAnswer4 = Answer("Séville", language.id).update()

	question = Question(
		"Quelle est la Capitale de l'Espagne ?",
		(Category.findByLabel("Géographie")).id,
		(Force.findByLabel("Moyen")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer1
	)
	questions.append(question)

	idAnswer1 = Answer("Sydney", language.id).update()
	idAnswer2 = Answer("Melbourne", language.id).update()
	idAnswer3 = Answer("Canberra", language.id).update()
	idAnswer4 = Answer("Adélaïde", language.id).update()

	question = Question(
		"Quelle est la Capitale de l'Australie ?",
		(Category.findByLabel("Géographie")).id,
		(Force.findByLabel("Moyen")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Saint-Pétersbourg", language.id).update()
	idAnswer2 = Answer("Moscou", language.id).update()
	idAnswer3 = Answer("Novossibirsk", language.id).update()
	idAnswer4 = Answer("Sotchi", language.id).update()
	
	question = Question(
		"Quelle est la Capitale de la Russie ?",
		(Category.findByLabel("Géographie")).id,
		(Force.findByLabel("Moyen")).id,
		language.id,
		idAnswer1,
		idAnswer2,
		idAnswer3,
		idAnswer4,
		idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Lewis Hamilton", language.id).update()
	idAnswer2 = Answer("Brandon Armstrong", language.id).update()
	idAnswer3 = Answer("Louis Armstrong", language.id).update()
	idAnswer4 = Answer("Neil Hamilton", language.id).update()

	question = Question(
		"Qui est un musicien\/chanteur américain de jazz ?",
		(Category.findByLabel("Musique")).id,
		(Force.findByLabel("Moyen")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("L'île d'Elbe", language.id).update()
	idAnswer2 = Answer("La Corse", language.id).update()
	idAnswer3 = Answer("La Sardaigne", language.id).update()
	idAnswer4 = Answer("L'île Sainte-Hélène", language.id).update()

	question = Question(
		"Sur quelle île est né Napoléon Banaparte ?",
		(Category.findByLabel("Histoire")).id,
		(Force.findByLabel("Moyen")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer2
	)
	questions.append(question)
	
	idAnswer1 = Answer("L'île d'Elbe", language.id).update()
	idAnswer2 = Answer("La Corse", language.id).update()
	idAnswer3 = Answer("La Sardaigne", language.id).update()
	idAnswer4 = Answer("L'île Sainte-Hélène", language.id).update()

	question = Question(
		"Sur quelle île est mort Napoléon Banaparte ?",
		(Category.findByLabel("Histoire")).id,
		(Force.findByLabel("Moyen")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer4
	)
	questions.append(question)

	idAnswer1 = Answer("Une espèce de poissons", language.id).update()
	idAnswer2 = Answer("Une espèce de corail", language.id).update()
	idAnswer3 = Answer("Une espèce d'étoiles de mer", language.id).update()
	idAnswer4 = Answer("Une espèce d'algues", language.id).update()

	question = Question(
		"Qu'est ce que l'Aquilonastra conandae ?",
		(Category.findByLabel("Faune et Flore")).id,
		(Force.findByLabel("Difficile")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("1978", language.id).update()
	idAnswer2 = Answer("1979", language.id).update()
	idAnswer3 = Answer("1980", language.id).update()
	idAnswer4 = Answer("1981", language.id).update()

	question = Question(
		"En quelle année Margaret Thatcher est-elle devenue 1er Ministre du Royaume-Uni ?",
		(Category.findByLabel("Politique")).id,
		(Force.findByLabel("Difficile")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer2
	)
	questions.append(question)

	idAnswer1 = Answer("1979", language.id).update()
	idAnswer2 = Answer("1981", language.id).update()
	idAnswer3 = Answer("1983", language.id).update()
	idAnswer4 = Answer("1985", language.id).update()

	question = Question(
		"En quelle année est né Franck Ribéry ?",
		(Category.findByLabel("Sport")).id,
		(Force.findByLabel("Difficile")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Joe, William, Jack et Averell", language.id).update()
	idAnswer2 = Answer("Joe, George, Jack et Averell", language.id).update()
	idAnswer3 = Answer("Joe, William, George et Averell", language.id).update()
	idAnswer4 = Answer("Joe, George, Jim et Averell", language.id).update()

	question = Question(
		"Dans Luky Luke, quels sont les prénoms des Dalton ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer1
	)
	questions.append(question)

	idAnswer1 = Answer("Nadia", language.id).update()
	idAnswer2 = Answer("Dumbo", language.id).update()
	idAnswer3 = Answer("Élodie", language.id).update()
	idAnswer4 = Answer("La Maîtresse", language.id).update()
	
	question = Question(
		"De qui Titeuf est-il amoureux ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
		idAnswer1,
		idAnswer2,
		idAnswer3,
		idAnswer4,
		idAnswer1
	)
	questions.append(question)

	idAnswer1 = Answer("Jenifer", language.id).update()
	idAnswer2 = Answer("Nolwenn Leroy", language.id).update()
	idAnswer3 = Answer("Élodie Frégé", language.id).update()
	idAnswer4 = Answer("Grégory Lemarchal", language.id).update()

	question = Question(
		"Qui a gagné la saison 1 de la Star Académy en France ?",
		(Category.findByLabel("Divertissement")).id,
		(Force.findByLabel("Moyen")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer1
	)
	questions.append(question)

	idAnswer1 = Answer("Jenifer", language.id).update()
	idAnswer2 = Answer("Nolwenn Leroy", language.id).update()
	idAnswer3 = Answer("Élodie Frégé", language.id).update()
	idAnswer4 = Answer("Grégory Lemarchal", language.id).update()

	question = Question(
		"Qui a gagné la saison 2 de la Star Académy en France ?",
		(Category.findByLabel("Divertissement")).id,
		(Force.findByLabel("Moyen")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer2
	)
	questions.append(question)
	
	idAnswer1 = Answer("Jenifer", language.id).update()
	idAnswer2 = Answer("Nolwenn Leroy", language.id).update()
	idAnswer3 = Answer("Élodie Frégé", language.id).update()
	idAnswer4 = Answer("Grégory Lemarchal", language.id).update()

	question = Question(
		"Qui a gagné la saison 3 de la Star Académy en France ?",
		(Category.findByLabel("Divertissement")).id,
		(Force.findByLabel("Moyen")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Jenifer", language.id).update()
	idAnswer2 = Answer("Nolwenn Leroy", language.id).update()
	idAnswer3 = Answer("Élodie Frégé", language.id).update()
	idAnswer4 = Answer("Grégory Lemarchal", language.id).update()

	question = Question(
		"Qui a gagné la saison 4 de la Star Académy en France ?",
		(Category.findByLabel("Divertissement")).id,
		(Force.findByLabel("Moyen")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer4
	)
	questions.append(question)

	idAnswer1 = Answer("France", language.id).update()
	idAnswer2 = Answer("Uruguay", language.id).update()
	idAnswer3 = Answer("Italie", language.id).update()
	idAnswer4 = Answer("Brésil", language.id).update()

	question = Question(
		"Où s'est déroulée la Coupe du Monde de football de 1930 ?",
		(Category.findByLabel("Sport")).id,
		(Force.findByLabel("Moyen")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer2
	)
	questions.append(question)

	idAnswer1 = Answer("Allemagne", language.id).update()
	idAnswer2 = Answer("Uruguay", language.id).update()
	idAnswer3 = Answer("Italie", language.id).update()
	idAnswer4 = Answer("Brésil", language.id).update()

	question = Question(
		"Quel pays a remporté la Coupe du Monde de football de 1930 ?",
		(Category.findByLabel("Sport")).id,
		(Force.findByLabel("Moyen")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer2
	)
	questions.append(question)

	idAnswer1 = Answer("France", language.id).update()
	idAnswer2 = Answer("Uruguay", language.id).update()
	idAnswer3 = Answer("Italie", language.id).update()
	idAnswer4 = Answer("Brésil", language.id).update()

	question = Question(
		"Où s'est déroulée la Coupe du Monde de football de 1934 ?",
		(Category.findByLabel("Sport")).id,
		(Force.findByLabel("Moyen")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Allemagne", language.id).update()
	idAnswer2 = Answer("Uruguay", language.id).update()
	idAnswer3 = Answer("Italie", language.id).update()
	idAnswer4 = Answer("Brésil", language.id).update()

	question = Question(
		"Quel pays a remporté la Coupe du Monde de football de 1934 ?",
		(Category.findByLabel("Sport")).id,
		(Force.findByLabel("Moyen")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Chausson", language.id).update()
	idAnswer2 = Answer("Soulier", language.id).update()
	idAnswer3 = Answer("Babouche", language.id).update()
	idAnswer4 = Answer("Mule", language.id).update()
	
	question = Question(
		"Comment s'appelle le singe, compagnon de Dora l'exploratrice ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
		idAnswer1,
		idAnswer2,
		idAnswer3,
		idAnswer4,
		idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Edith CRESSON", language.id).update()
	idAnswer2 = Answer("Ségolène ROYAL", language.id).update()
	idAnswer3 = Answer("Simone VEIL", language.id).update()
	idAnswer4 = Answer("Michèle ALLIOT-MARIE", language.id).update()

	question = Question(
		"En France, qui a été la première femme 1er Ministre ?",
		(Category.findByLabel("Politique")).id,
		(Force.findByLabel("Moyen")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer1
	)
	questions.append(question)

	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)
	
	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Chausson", language.id).update()
	idAnswer2 = Answer("Soulier", language.id).update()
	idAnswer3 = Answer("Babouche", language.id).update()
	idAnswer4 = Answer("Mule", language.id).update()
	
	question = Question(
		"Comment s'appelle le singe, compagnon de Dora l'exploratrice ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
		idAnswer1,
		idAnswer2,
		idAnswer3,
		idAnswer4,
		idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Edith CRESSON", language.id).update()
	idAnswer2 = Answer("Ségolène ROYAL", language.id).update()
	idAnswer3 = Answer("Simone VEIL", language.id).update()
	idAnswer4 = Answer("Michèle ALLIOT-MARIE", language.id).update()

	question = Question(
		"En France, qui a été la première femme 1er Ministre ?",
		(Category.findByLabel("Politique")).id,
		(Force.findByLabel("Moyen")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer1
	)
	questions.append(question)

	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)
	
	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Chausson", language.id).update()
	idAnswer2 = Answer("Soulier", language.id).update()
	idAnswer3 = Answer("Babouche", language.id).update()
	idAnswer4 = Answer("Mule", language.id).update()
	
	question = Question(
		"Comment s'appelle le singe, compagnon de Dora l'exploratrice ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
		idAnswer1,
		idAnswer2,
		idAnswer3,
		idAnswer4,
		idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Edith CRESSON", language.id).update()
	idAnswer2 = Answer("Ségolène ROYAL", language.id).update()
	idAnswer3 = Answer("Simone VEIL", language.id).update()
	idAnswer4 = Answer("Michèle ALLIOT-MARIE", language.id).update()

	question = Question(
		"En France, qui a été la première femme 1er Ministre ?",
		(Category.findByLabel("Politique")).id,
		(Force.findByLabel("Moyen")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer1
	)
	questions.append(question)

	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)
	
	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Chausson", language.id).update()
	idAnswer2 = Answer("Soulier", language.id).update()
	idAnswer3 = Answer("Babouche", language.id).update()
	idAnswer4 = Answer("Mule", language.id).update()
	
	question = Question(
		"Comment s'appelle le singe, compagnon de Dora l'exploratrice ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
		idAnswer1,
		idAnswer2,
		idAnswer3,
		idAnswer4,
		idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Edith CRESSON", language.id).update()
	idAnswer2 = Answer("Ségolène ROYAL", language.id).update()
	idAnswer3 = Answer("Simone VEIL", language.id).update()
	idAnswer4 = Answer("Michèle ALLIOT-MARIE", language.id).update()

	question = Question(
		"En France, qui a été la première femme 1er Ministre ?",
		(Category.findByLabel("Politique")).id,
		(Force.findByLabel("Moyen")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer1
	)
	questions.append(question)

	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)
	
	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Chausson", language.id).update()
	idAnswer2 = Answer("Soulier", language.id).update()
	idAnswer3 = Answer("Babouche", language.id).update()
	idAnswer4 = Answer("Mule", language.id).update()
	
	question = Question(
		"Comment s'appelle le singe, compagnon de Dora l'exploratrice ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
		idAnswer1,
		idAnswer2,
		idAnswer3,
		idAnswer4,
		idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Edith CRESSON", language.id).update()
	idAnswer2 = Answer("Ségolène ROYAL", language.id).update()
	idAnswer3 = Answer("Simone VEIL", language.id).update()
	idAnswer4 = Answer("Michèle ALLIOT-MARIE", language.id).update()

	question = Question(
		"En France, qui a été la première femme 1er Ministre ?",
		(Category.findByLabel("Politique")).id,
		(Force.findByLabel("Moyen")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer1
	)
	questions.append(question)

	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)
	
	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	idAnswer1 = Answer("Un artichaud", language.id).update()
	idAnswer2 = Answer("Un radis", language.id).update()
	idAnswer3 = Answer("Un poireau", language.id).update()
	idAnswer4 = Answer("Une tomate", language.id).update()

	question = Question(
		"Qu'utilise le pokémon Canarticho pour attaquer ?",
		(Category.findByLabel("Enfant")).id,
		(Force.findByLabel("Enfant")).id,
		language.id,
	 	idAnswer1,
	 	idAnswer2,
	 	idAnswer3,
	 	idAnswer4,
	 	idAnswer3
	)
	questions.append(question)

	return questions

def questionsForEnglish(language):
	questions = []
	
	# idAnswer1 = Answer("Answer 111", language.id).update()
	# idAnswer2 = Answer("Answer 2", language.id).update()
	# idAnswer3 = Answer("Answer 3", language.id).update()
	# idAnswer4 = Answer("Answer 4", language.id).update()
	
	# question = Question(
	# 	"First question ?",
	# 	(Category.findByLabel("Kind")).id,
	# 	(Force.findByLabel("Kind")).id,
	# 	language.id,
	# 	idAnswer1,
	# 	idAnswer2,
	# 	idAnswer3,
	# 	idAnswer4,
	# 	idAnswer3
	# )
	# questions.append(question)

	return questions

def createQuestionsForLanguage(language):
	print("... For " + language.label)
	questions = []

	if language.label == "Français":
		questions = questionsForFrench(language)

	if language.label == "English":
		questions = questionsForEnglish(language)

	if questions is not None:
		for question in questions:
			# if Question.findByLabel(question.label) is None:
			print(question)
			question.updated_by = 1
			question.update()


def createQuestions(languages):
	print("Create Questions...")
	for language in languages:
		createQuestionsForLanguage(language)

	print("... OK.")

print("Create référentials...")
createLanguages()
languages = Language.getAll()
createForces(languages)
createCategories(languages)
createQuestions(languages)
print("référentials are OK.")
